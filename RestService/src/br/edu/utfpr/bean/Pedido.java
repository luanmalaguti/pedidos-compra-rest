package br.edu.utfpr.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import br.edu.utfpr.bean.ibean.Bean;

@Entity
@Table(name = "Pedidos")
@NamedQuery(name = "Pedido.list", query = "select p from Pedido p")
@XmlRootElement(name="Pedido")
public class Pedido implements Bean{

	public void setItemPedido(List<ItemPedido> itemPedido) {
		this.itemPedido = itemPedido;
	}

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String dataConvert;
	
	@OneToMany(cascade =  CascadeType.ALL)
	private List<ItemPedido> itemPedido;
	
	public Pedido() {
		itemPedido = new ArrayList<ItemPedido>();
	}
	
	public Pedido(String data) {
		this.setDataConvert(data);
		itemPedido = new ArrayList<ItemPedido>();
	}
	
	public Pedido(Long id, String dataConvert) {
		this.id = id;
		this.dataConvert = dataConvert;
		itemPedido = new ArrayList<ItemPedido>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void addItemPedido(ItemPedido itemPedido){
		this.itemPedido.add(itemPedido);
	}
	
	public List<ItemPedido> getItems(){
		return this.itemPedido;
	}
	
	public String getDataConvert() {
		return this.dataConvert;
	}

	public void setDataConvert(String dataConvert) {
		this.dataConvert = dataConvert;
	}
}
