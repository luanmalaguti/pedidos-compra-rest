package br.edu.utfpr.persistece;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Singleton que promove uma única instancia do contexto de persistência
 */
public class PersistenceUtil {

    protected static EntityManagerFactory emf = null;
    protected static EntityManager em = null;

    private PersistenceUtil() {
        emf = Persistence.createEntityManagerFactory("persistence-base");
        em = emf.createEntityManager();
    }

    public static EntityManager getInstance(){
        if(em == null){
            new PersistenceUtil();
        }
        return em;
    }

}