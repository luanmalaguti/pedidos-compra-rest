package br.edu.utfpr.rest.resources;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import br.edu.utfpr.bean.User;
import br.edu.utfpr.dao.GenericDAO;

@Path("/user")
public class UserResource {

	private GenericDAO<User> dao = new GenericDAO<User>(User.class);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getUsers() {
		return dao.list();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)	
	@Path("/logar")
	public User logar(@FormParam(value = "name") String name,
			@FormParam(value = "password") String password,
			@Context HttpServletResponse response) {
		System.out.println("Entrou no Logar do REST");
		List<User> users = dao.list();
		for (User user : users) {
			if(user.getName().equalsIgnoreCase(name) && (user.getPassword().equals(password))){
				return user;
			}
		}
		return null;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public User saveUser(@FormParam(value = "name") String name,
			@FormParam(value = "email") String email,
			@FormParam(value = "password") String password,
			@Context HttpServletResponse response) throws IOException {

		User u = new User(name, email, password);
		System.out.println("Entrou no Rest Bostão");
		dao.save(u);
		return u;
	}

	@DELETE
	@Produces(MediaType.TEXT_XML)
	@Path("/{id}")
	public void remove(@PathParam("id") String id) {
		System.out.println("Método Delete ID: " + id);
		dao.delete(dao.find(Long.parseLong(id)));
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/{id}")
	public User updateUser(@PathParam("id") String id,
			@FormParam(value = "name") String name,
			@FormParam(value = "email") String email,
			@FormParam(value = "password") String password,
			@Context HttpServletResponse response) throws IOException {

		User u = dao.find(Long.parseLong(id));
		u.setName(name);
		u.setEmail(email);
		u.setPassword(password);
		System.out.println("Entrou no Rest Bostão");
		dao.save(u);
		return u;
	}
}
