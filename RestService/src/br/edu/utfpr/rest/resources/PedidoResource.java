package br.edu.utfpr.rest.resources;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.edu.utfpr.bean.ItemPedido;
import br.edu.utfpr.bean.Pedido;
import br.edu.utfpr.bean.User;
import br.edu.utfpr.dao.GenericDAO;

import com.google.gson.Gson;

@Path("/pedido")
public class PedidoResource {

	private GenericDAO<Pedido> dao = new GenericDAO<Pedido>(Pedido.class);
	private GenericDAO<User> daoUser = new GenericDAO<User>(User.class);
	private Gson gson = new Gson();
	private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Pedido> getPedidos() {
		List<Pedido> pedidos = new ArrayList<Pedido>();
		//gambia feia para corrigir erro maluco do gson
		for (Pedido p : dao.list()) {
			pedidos.add(new Pedido(p.getId(), p.getDataConvert()));
		}
		return pedidos;
	}

	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void save(@FormParam(value = "pedido") String json){
		try {
			System.out.println("Recebido no servidor: "+json);
			//Monta o pedido
			Pedido pedido = gson.fromJson(json, Pedido.class);
			//Guardaos items do pedido temporaiamente 
			List<ItemPedido> items = pedido.getItems();
			//Reseta a lista de pedidos 
			pedido.setItemPedido(new ArrayList<ItemPedido>());
			//Persiste para obter um Id
			pedido = dao.save(pedido);
			
			//Adiciona os dados do pedido atualizado na relação pedido/produto
			for(ItemPedido ip : items){
				ip.setPedidoId(pedido.getId());
				ip.setPedido(pedido);
				
				pedido.addItemPedido(ip);
			}
			
			//Persiste novamente o pedido, agora já com a lista de items
			pedido = dao.save(pedido);
			
			User u = daoUser.find(1L);
			//Relaciona o pedido ao usuario
			u.addPedido(pedido);
			//Finalmente persiste o usuario
			daoUser.save(u);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
