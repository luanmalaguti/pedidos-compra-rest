package br.edu.utfpr.rest.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import br.edu.utfpr.bean.ItemPedido;
import br.edu.utfpr.bean.Pedido;
import br.edu.utfpr.bean.Produto;
import br.edu.utfpr.bean.wrapper.Produtos;
import br.edu.utfpr.dao.GenericDAO;

@Path("/produto")
public class ProdutoResource {

	private GenericDAO<Produto> dao = new GenericDAO<Produto>(Produto.class);
	private GenericDAO<Pedido> daoPedido = new GenericDAO<Pedido>(Pedido.class);
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Produtos getProdutos() {
		Produtos wrapper = new Produtos();
		wrapper.setProdutos(dao.list());
		return wrapper;
	}
	
	@POST
	@Path("/bypedido")
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Produtos getProdutosByPedido(@FormParam(value = "id") String id){
		List<Produto> produtos = new ArrayList<Produto>();
		Produtos wrapper = new Produtos();
		
		for (Pedido pedido : daoPedido.list()) {
			if(pedido.getId().equals(Long.parseLong(id))){
				for(ItemPedido ip : pedido.getItems()){
					produtos.add(ip.getProduto());	
				}
			}
		}
		
		
		wrapper.setProdutos(produtos);
		return wrapper;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Produto saveProduto(
			@FormParam(value = "descricao") String descricao,
			@FormParam(value = "valorUnitario") String val,
			@FormParam(value = "unidadeMedida") String unidade,
			@FormParam(value = "tamanho") String tamanho,
			@Context HttpServletResponse response) throws IOException {

		Produto p = new Produto(descricao, Double.parseDouble(val), unidade,
				Double.valueOf(tamanho));
		System.out.println("Entrou no Rest Bostão!!!");
		dao.save(p);
		return p;
	}
	
	@DELETE
	@Produces(MediaType.TEXT_XML)
	@Path("/{id}")
	public void remove(@PathParam("id") String id) {
		System.out.println("Método Delete ID: " + id);
		dao.delete(dao.find(Long.parseLong(id)));
	}	
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/{id}")
	public Produto updateProduct(@PathParam("id") String id,
			@FormParam(value = "descricao") String descricao,
			@FormParam(value = "valorUnitario") String val,
			@FormParam(value = "unidadeMedida") String unidade,
			@FormParam(value = "tamanho") String tamanho,
			@Context HttpServletResponse response) throws IOException {

		Produto pro = dao.find(Long.parseLong(id));
		pro.setDescricao(descricao);
		pro.setUnidadeMedida(unidade);
		pro.setValorUnitario(Double.parseDouble(val));
		pro.setTamanho(Double.parseDouble(tamanho));
		
		System.out.println("Entrou no Rest Bostão");
		dao.save(pro);
		return pro;
	}
}
