package br.edu.utfpr.rest.application;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import br.edu.utfpr.rest.resources.PedidoResource;
import br.edu.utfpr.rest.resources.ProdutoResource;
import br.edu.utfpr.rest.resources.UserResource;

@ApplicationPath("/rest")
public class RestApplication extends Application{
	
	private final Set<Class<?>> classes;
	
	public RestApplication() {
		HashSet<Class<?>> c = new HashSet<Class<?>>();
		
		c.add(UserResource.class);
		c.add(ProdutoResource.class);
		c.add(PedidoResource.class);
		
		classes = Collections.unmodifiableSet(c);
	}
	
	public Set<Class<?>> getClasses(){
		return classes;
	}
}
