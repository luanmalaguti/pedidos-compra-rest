package br.edu.utfpr.bean.wrapper;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import br.edu.utfpr.bean.Produto;

@XmlRootElement
public class Produtos {
	private List<Produto> produtos = new ArrayList<Produto>();

	public Produtos() {
	}
	
	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}
	
	
}
