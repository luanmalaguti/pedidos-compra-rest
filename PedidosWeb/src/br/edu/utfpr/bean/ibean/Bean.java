package br.edu.utfpr.bean.ibean;

import java.io.Serializable;
/**
 * Interface a ser implementada pelos Beans persistíveis.
 */
public interface Bean extends Serializable {
    Long getId();
    void setId(Long id);
}