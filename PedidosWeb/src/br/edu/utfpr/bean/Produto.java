package br.edu.utfpr.bean;

import java.util.ArrayList;
import java.util.List;

import br.edu.utfpr.bean.ibean.Bean;

public class Produto implements Bean {
	
	private static final long serialVersionUID = 1L;

    private Long id;
    private String descricao;
    private double valorUnitario;
    private String unidadeMedida;
    private double tamanho;
    
	private List<ItemPedido> itemPedido;
    
    public Produto() {
		itemPedido = new ArrayList<ItemPedido>();
	}
    
	public Produto(String descricao, double valorUnitario, String unidadeMedida, double tamanho) {
		this.descricao = descricao;
		this.valorUnitario = valorUnitario;
		this.unidadeMedida = unidadeMedida;
		this.tamanho = tamanho;
		
		itemPedido = new ArrayList<ItemPedido>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public double getTamanho() {
		return tamanho;
	}

	public void setTamanho(double tamanho) {
		this.tamanho = tamanho;
	}

	public List<ItemPedido> getItemPedido() {
		return itemPedido;
	}

	public void setItemPedido(List<ItemPedido> itemPedido) {
		this.itemPedido = itemPedido;
	}
    
}
