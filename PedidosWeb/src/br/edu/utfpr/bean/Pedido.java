package br.edu.utfpr.bean;

import java.util.ArrayList;
import java.util.List;

import br.edu.utfpr.bean.ibean.Bean;

public class Pedido implements Bean{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String dataConvert;
	
	private List<ItemPedido> itemPedido;
	
	public Pedido() {
		itemPedido = new ArrayList<ItemPedido>();
	}
	
	public Pedido(String data) {
		this.setDataConvert(data);
		itemPedido = new ArrayList<ItemPedido>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
		
	public void addItemPedido(ItemPedido itemPedido){
		this.itemPedido.add(itemPedido);
	}
	
	public List<ItemPedido> getItems(){
		return this.itemPedido;
	}

	public void setItemPedido(List<ItemPedido> itemPedido) {
		this.itemPedido = itemPedido;
	}
	
	public String getDataConvert() {
		return this.dataConvert;
	}

	public void setDataConvert(String dataConvert) {
		this.dataConvert = dataConvert;
	}
}
