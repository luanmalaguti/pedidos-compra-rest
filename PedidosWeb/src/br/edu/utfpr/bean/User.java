package br.edu.utfpr.bean;

import java.util.ArrayList;
import java.util.List;

import br.edu.utfpr.bean.ibean.Bean;

public class User implements Bean{

    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String email;
    private String password;
    private boolean admin;
    
    private List<Pedido> pedidos;

    public User() {
    	pedidos = new ArrayList<Pedido>();
    }

    public User(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
        
        pedidos = new ArrayList<Pedido>();
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	public void addPedido(Pedido p){
		this.pedidos.add(p);
	}
	
	public void removePedido(Pedido p){
		this.pedidos.remove(p);
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}
}