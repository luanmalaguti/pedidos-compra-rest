package br.edu.utfpr.mb;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.edu.utfpr.bean.User;
import br.edu.utfpr.controller.LoginController;

@ManagedBean(name = "LoginMB")
@SessionScoped
public class LoginMB implements Serializable {

	private static final long serialVersionUID = 1L;

	private LoginController controller;
	private User user;

	public LoginMB() {
		this.user = new User();
		this.controller = new LoginController();
	}

	public String logar() {
		this.user = controller.logar(user);

		if (user == null) {
			novo();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL, "Erro de validacao:",
					"Usuario e/ou senha invalidos."));
			context.getExternalContext().getFlash().setKeepMessages(true);
		}
		return "index.xhtml";
	}
	
	public String logout(){
		controller.logout();
		novo();
		System.out.println("Logout Alucinante");
		return "Index.xhtml";
	}

	public void novo() {
		this.user = new User();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
