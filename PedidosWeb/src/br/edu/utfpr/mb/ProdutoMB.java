package br.edu.utfpr.mb;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.edu.utfpr.bean.Produto;
import br.edu.utfpr.controller.ProdutoController;

@ManagedBean(name="ProdutoMB")
@SessionScoped
public class ProdutoMB implements Serializable {
	
	//############ Attributes ############
	
	private static final long serialVersionUID = 1L;
	private ProdutoController controller = new ProdutoController();
	private List<Produto> produtos = controller.getAll();
	
	private Produto produto = new Produto();
	
	//############ Actions ############
	
	@PostConstruct
	public void init(){
		novo();
		produtos = controller.getAll();
	}
	
	public ProdutoMB() {
		novo();
	}
	
	public String save(){
		if(produto.getId() != null)
			controller.update(produto);
		else 
			controller.save(produto);
		novo();
		return "ProdutosAtualizar.xhtml";
	}
	
	public void novo(){
		produto = new Produto();
	}
	
	public String edit(Produto t) {
		this.produto = t;
		return "ProdutoCad.xhtml";
	}
	
	public String remove(Produto t) {
		controller.remove(t);
		System.out.println("Excluir.");
		return "#";
	}
	
	
	//############ Getter and Setters ############
	
	public List<Produto> getProdutos() {
		return produtos;
	}
	
	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}
	
	public Produto getProduto() {
		return produto;
	}
	
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
}
