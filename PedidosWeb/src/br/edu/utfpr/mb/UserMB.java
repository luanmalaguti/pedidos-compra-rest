package br.edu.utfpr.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.edu.utfpr.bean.User;
import br.edu.utfpr.controller.UserController;

@ManagedBean(name = "UserMB")
@SessionScoped
public class UserMB implements Serializable {

	private static final long serialVersionUID = 1L;

	private UserController controller = new UserController();
	private List<User> users = new ArrayList<User>();;
	private User user;

	@PostConstruct
	public void init() {
		users = controller.getAll();
	}

	public UserMB() {
		this.user = new User();
		this.users = new ArrayList<User>();
	}

	public String save() {
		System.out.println("ID: " + user.getId());
		if (user.getId() != null)
			controller.update(user);
		else
			controller.save(user);
		novo();
		return "Users.xhtml";
	}
	
	public void novo(){
		this.user = new User();
	}

	public String remove(User t) {
		controller.remove(t);
		System.out.println("Excluir.");
		return "#";
	}

	public String edit(User t) {
		this.user = t;
		return "UserCad.xhtml";
	}

	public UserController getController() {
		return controller;
	}

	public void setController(UserController controller) {
		this.controller = controller;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
