package br.edu.utfpr.mb;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.edu.utfpr.bean.ItemPedido;
import br.edu.utfpr.bean.Pedido;
import br.edu.utfpr.bean.Produto;
import br.edu.utfpr.bean.User;
import br.edu.utfpr.controller.PedidoController;
import br.edu.utfpr.controller.ProdutoController;

@ManagedBean(name="PedidoMB")
@SessionScoped
public class PedidoMB implements Serializable{
	
	//############ Attributes ############
	
	private static final long serialVersionUID = 1L;

	private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");
	private PedidoController controller = new PedidoController(new User());
	private ProdutoController produtoController = new ProdutoController();
	private List<Produto> produtosPedido = new ArrayList<Produto>();
	private List<Pedido> pedidos = new ArrayList<Pedido>();
	private Pedido pedido = new Pedido();
	
	private Pedido selecionado = new Pedido();
	private List<Produto> produtosSelecionado = new ArrayList<Produto>();
	
	private double total = 0.0;
	
	//############ Actions ############
	
	@PostConstruct
	public void init(){
		System.out.println("Pedido.init()");
		pedidos = controller.getAll();
	}
	
	public String addProduto(Produto produto){
		produtosPedido.add(produto);
		updateTotal();
		return "Pedido.xhtml";
	}
	
	public void remove(Produto produto){
		produtosPedido.remove(produto);
		updateTotal();
	}
	
	public void updateTotal(){
		total = 0.0;
		for (Produto p : produtosPedido) {
			total = total + p.getValorUnitario();
		}
	}
	
	public String novoPedido(){
		controller = new PedidoController(new User());
		List<ItemPedido> itemsPedido = new ArrayList<ItemPedido>();
		pedido = new Pedido();
		pedido.setDataConvert(format.format(Calendar.getInstance().getTime()));
		
		for(Produto p : produtosPedido){
			ItemPedido ip = new ItemPedido();
			
			ip.setProdutoId(p.getId());
			ip.setProduto(p);
			ip.setValorUnitario(p.getValorUnitario());
			
			pedido.addItemPedido(ip);
		}
		
		controller.save(pedido);
		novo();
		return "Index.xhtml";
	}
	
	public String updateSelecionado(Pedido selecionado) {
		System.out.println("updateSelecionado");
		this.selecionado = selecionado;
		produtosSelecionado.clear();
		produtosSelecionado = produtoController.getAllByPedido(selecionado.getId());
		
		for (Produto p : produtosSelecionado) {
			System.out.println(p.getDescricao());
		}
		
		return "ProdutosPedido.xhtml";
	}
	
	public void novo(){
		pedido = new Pedido();
		produtosPedido.clear();
		produtosSelecionado.clear();
	}
	
	//############ Getter and Setters ############
	
	public List<Produto> getProdutosPedido() {
		return produtosPedido;
	}

	public void setProdutosPedido(List<Produto> produtosPedido) {
		this.produtosPedido = produtosPedido;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public Pedido getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(Pedido selecionado) {
		this.selecionado = selecionado;
	}
	
	public List<Produto> getProdutosSelecionado() {
		return produtosSelecionado;
	}

	public void setProdutosSelecionado(List<Produto> produtosSelecionado) {
		this.produtosSelecionado = produtosSelecionado;
	}
	
}
