package br.edu.utfpr.controller;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.Application;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import br.edu.utfpr.bean.Pedido;
import br.edu.utfpr.bean.User;

public class PedidoController extends AbstractController<Pedido>{
	
	private User user;
	
	public PedidoController(User user) {
		this.user = user;
	}
	
	@Override
	public Pedido save(Pedido p) {
		Form form = new Form();
		form.param("pedido", gson.toJson(p));
		res = target
				.path("pedido")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(form,
						MediaType.APPLICATION_FORM_URLENCODED));

		System.out.println("--> Status: " + res.getStatus());
		System.out.println("--> Status info: " + res.getStatusInfo());
		System.out.println("--> Cabecalho: " + res.getStringHeaders());
		refresh();
		return null;
	}

	@Override
	Pedido update(Pedido t) {
		return null;
	}

	@Override
	public List<Pedido> getAll() {
		res = target.path("pedido").request(MediaType.APPLICATION_JSON).get();
		String re = res.readEntity(String.class);
		System.out.println("-----Resposta ---> " + re);
		refresh();
		return jSonToList(re);
	}

	@Override
	void remove(Pedido t) {
		
	}

	@Override
	Pedido find(Pedido t) {
		return null;
	}
	
	private List<Pedido> jSonToList(String json) {
		JsonElement jsonNew;
		try {
			jsonNew = new JsonParser().parse(json);
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
			JsonReader reader = new JsonReader(new StringReader(json));
			reader.setLenient(true);
			jsonNew = new JsonParser().parse(reader);
		}

		JsonArray array = jsonNew.getAsJsonArray();

		Iterator iterator = array.iterator();
		List<Pedido> pedidos = new ArrayList<Pedido>();
		while (iterator.hasNext()) {
			JsonElement json2 = (JsonElement) iterator.next();
			Pedido p = gson.fromJson(json2, Pedido.class);
			pedidos.add(p);
		}
		for (Pedido pedido : pedidos) {
			for (Pedido p : pedidos) {
				System.out.println("Recebido: "+p.getId() +" : " +p.getItems().size());
			}
		}
		return pedidos;
	}
	
	public void refresh() {
		FacesContext context = FacesContext.getCurrentInstance();
		Application application = context.getApplication();
		ViewHandler viewHandler = application.getViewHandler();
		UIViewRoot viewRoot = viewHandler.createView(context, context
				.getViewRoot().getViewId());
		context.setViewRoot(viewRoot);
		context.renderResponse();
	}


}
