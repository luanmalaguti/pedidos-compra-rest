package br.edu.utfpr.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.Application;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.filter.HttpBasicAuthFilter;

import br.edu.utfpr.bean.User;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class LoginController extends AbstractController<User> implements
		Serializable {

	private static final long serialVersionUID = 1L;

	public User logar(User u) {
		Form form = new Form();
		form.param("name", u.getName());
		form.param("password", u.getPassword());

		res = target
				.path("user/logar")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(form,
						MediaType.APPLICATION_FORM_URLENCODED_TYPE));

		String p = res.readEntity(String.class);
		JsonElement jsonNew = new JsonParser().parse(p);
		u = gson.fromJson(jsonNew, User.class);

		if (u != null) {
			System.out.println("Usuário Autorizado via WS: " + u.getId()
					+ " - " + u.getName());

			HttpSession session = (HttpSession) FacesContext
					.getCurrentInstance().getExternalContext()
					.getSession(false);

			session.setAttribute("name", u.getName());
			session.setAttribute("password", u.getPassword());

			target.register(new HttpBasicAuthFilter(String.valueOf(session
					.getAttribute("name")), String.valueOf(session
					.getAttribute("password"))));
			System.out.println("Usuario da sessao: "
					+ String.valueOf(session.getAttribute("name")));
			System.out.println("Senha do usuario: "
					+ String.valueOf(session.getAttribute("password")));
		}

		return u;
	}

	public void logout() {
		session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(false);
		session.setAttribute("name", null);
		session.setAttribute("password", null);
		System.out.println("Fim do logout no Controller");
		refresh();
	}

	public void refresh() {
		FacesContext context = FacesContext.getCurrentInstance();
		Application application = context.getApplication();
		ViewHandler viewHandler = application.getViewHandler();
		UIViewRoot viewRoot = viewHandler.createView(context, context
				.getViewRoot().getViewId());
		context.setViewRoot(viewRoot);
		context.renderResponse();
	}

	@Override
	User save(User t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	User update(User t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	List<User> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	void remove(User t) {
		// TODO Auto-generated method stub

	}

	@Override
	User find(User t) {
		// TODO Auto-generated method stub
		return null;
	}
}
