package br.edu.utfpr.controller;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.Application;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.filter.HttpBasicAuthFilter;

import br.edu.utfpr.bean.User;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

public class UserController extends AbstractController<User> implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public User save(User t) {
		Form form = new Form();
		form.param("name", t.getName());
		form.param("email", t.getEmail());
		form.param("password", t.getPassword());

		if (filter == null) {
			filter = new HttpBasicAuthFilter(String.valueOf(session
					.getAttribute("name")), String.valueOf(session
					.getAttribute("password")));
			target.register(filter);
		}

		res = target
				.path("user")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(form,
						MediaType.APPLICATION_FORM_URLENCODED_TYPE));

		System.out.println("--> Status: " + res.getStatus());
		System.out.println("--> Status info: " + res.getStatusInfo());
		System.out.println("--> Cabecalho: " + res.getStringHeaders());
		String str = res.readEntity(String.class);
		System.out.println("--> Resposta:" + str);

		JsonElement json2 = new JsonParser().parse(str);
		User u = gson.fromJson(json2, User.class);

		return u;
	}

	@Override
	public List<User> getAll() {

		if (filter == null) {
			filter = new HttpBasicAuthFilter(String.valueOf(session
					.getAttribute("name")), String.valueOf(session
					.getAttribute("password")));
			target.register(filter);
		}
		
		res = target.path("user").request(MediaType.APPLICATION_JSON).get();
		String re = res.readEntity(String.class);
		System.out.println("-----Resposta ---> " + re);
		return jSonToList(re);
	}

	@Override
	public void remove(User t) {

		if (filter == null) {
			filter = new HttpBasicAuthFilter(String.valueOf(session
					.getAttribute("name")), String.valueOf(session
					.getAttribute("password")));
			target.register(filter);
		}
		res = target.path("user").path(String.valueOf(t.getId()))
				.request(MediaType.TEXT_XML).delete();
		System.out.println("Remove do Controller");
		refresh();
	}

	public void refresh() {
		FacesContext context = FacesContext.getCurrentInstance();
		Application application = context.getApplication();
		ViewHandler viewHandler = application.getViewHandler();
		UIViewRoot viewRoot = viewHandler.createView(context, context
				.getViewRoot().getViewId());
		context.setViewRoot(viewRoot);
		context.renderResponse();
	}

	@Override
	public User find(User t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User update(User t) {
		Form form = new Form();
		form.param("name", t.getName());
		form.param("email", t.getEmail());
		form.param("password", t.getPassword());

		if (filter == null) {
			filter = new HttpBasicAuthFilter(String.valueOf(session
					.getAttribute("name")), String.valueOf(session
					.getAttribute("password")));
			target.register(filter);
		}
		res = target
				.path("user")
				.path(String.valueOf(t.getId()))
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(form,
						MediaType.APPLICATION_FORM_URLENCODED_TYPE));

		System.out.println("--> Status: " + res.getStatus());
		System.out.println("--> Status info: " + res.getStatusInfo());
		System.out.println("--> Cabecalho: " + res.getStringHeaders());
		String str = res.readEntity(String.class);
		System.out.println("--> Resposta:" + str);
		JsonElement json2 = new JsonParser().parse(str);
		User u = gson.fromJson(json2, User.class);

		return u;
	}

	public List<User> jSonToList(String json) {

		// JsonElement jsonNew = new JsonParser().parse(json);
		JsonElement jsonNew;
		try {
			jsonNew = new JsonParser().parse(json);
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
			JsonReader reader = new JsonReader(new StringReader(json));
			reader.setLenient(true);
			jsonNew = new JsonParser().parse(reader);
		}

		JsonArray array = jsonNew.getAsJsonArray();

		Iterator iterator = array.iterator();
		List<User> users = new ArrayList<User>();
		while (iterator.hasNext()) {
			JsonElement json2 = (JsonElement) iterator.next();
			User u = gson.fromJson(json2, User.class);
			users.add(u);
		}
		for (User u : users) {
			System.out.println("Recebido: " + u.getId() + " : " + u.getName());
		}
		return users;
	}

}
