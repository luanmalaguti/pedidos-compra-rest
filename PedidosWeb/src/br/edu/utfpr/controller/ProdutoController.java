package br.edu.utfpr.controller;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import br.edu.utfpr.bean.Produto;
import br.edu.utfpr.bean.wrapper.Produtos;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class ProdutoController extends AbstractController<Produto> implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public List<Produto> getAll() {
		System.out.println("Cambioo");
		res = target.path("produto")
				.request(MediaType.APPLICATION_XML).get();
		JAXBContext jaxb;
		try {
			jaxb = JAXBContext.newInstance(Produtos.class, Produto.class);
			Unmarshaller unmarshaller = jaxb.createUnmarshaller();
			StringReader stringReader = new StringReader(res.readEntity(
					String.class).toString());
			Produtos produtos = (Produtos) unmarshaller.unmarshal(stringReader);
			return produtos.getProdutos();
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return new ArrayList<Produto>();
	}

	@Override
	public Produto save(Produto t) {

		Form form = new Form();
		form.param("descricao", t.getDescricao());
		form.param("unidadeMedida", t.getUnidadeMedida());
		form.param("valorUnitario", String.valueOf(t.getValorUnitario()));
		form.param("tamanho", String.valueOf(t.getTamanho()));

		res = target
				.path("produto")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(form,
						MediaType.APPLICATION_FORM_URLENCODED_TYPE));

		System.out.println("--> Status: " + res.getStatus());
		System.out.println("--> Status info: " + res.getStatusInfo());
		System.out.println("--> Cabecalho: " + res.getStringHeaders());

		String p = res.readEntity(String.class);
		JsonElement jsonNew = new JsonParser().parse(p);
		Produto pro = gson.fromJson(jsonNew, Produto.class);
		if (res != null && res.getStatusInfo().equals(Response.ok()))
			return pro;
		return pro;
	}

	@Override
	public Produto update(Produto t) {
		Form form = new Form();
		form.param("descricao", t.getDescricao());
		form.param("unidadeMedida", t.getUnidadeMedida());
		form.param("valorUnitario", String.valueOf(t.getValorUnitario()));
		form.param("tamanho", String.valueOf(t.getTamanho()));

		res = target
				.path("produto")
				.path(String.valueOf(t.getId()))
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(form,
						MediaType.APPLICATION_FORM_URLENCODED_TYPE));

		System.out.println("--> Status: " + res.getStatus());
		System.out.println("--> Status info: " + res.getStatusInfo());
		System.out.println("--> Cabecalho: " + res.getStringHeaders());

		String p = res.readEntity(String.class);
		JsonElement jsonNew = new JsonParser().parse(p);
		Produto pro = gson.fromJson(jsonNew, Produto.class);
		if (res != null && res.getStatusInfo().equals(Response.ok()))
			return pro;
		return pro;
	}

	@Override
	public void remove(Produto t) {
		res = target.path("produto").path(String.valueOf(t.getId()))
				.request(MediaType.TEXT_XML).delete();
		System.out.println("Remove do Controller");
	}

	@Override
	public Produto find(Produto t) {
		return null;
	}
	
	public List<Produto> getAllByPedido(Long id) {
		Form form = new Form();
		form.param("id", String.valueOf(id));
		res = target.path("produto/bypedido").request(MediaType.APPLICATION_XML).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
		
		JAXBContext jaxb;
		try {
			jaxb = JAXBContext.newInstance(Produtos.class, Produto.class);
			Unmarshaller unmarshaller = jaxb.createUnmarshaller();
			StringReader stringReader = new StringReader(res.readEntity(
					String.class).toString());
			Produtos produtos = (Produtos) unmarshaller.unmarshal(stringReader);
			return produtos.getProdutos();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
		return new ArrayList<Produto>();
	}

}
