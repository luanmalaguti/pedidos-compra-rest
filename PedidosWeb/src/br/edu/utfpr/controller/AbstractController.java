package br.edu.utfpr.controller;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.filter.HttpBasicAuthFilter;

import com.google.gson.Gson;

public abstract class AbstractController<T> {

	protected Client client = ClientBuilder.newClient();
	protected WebTarget target = client
			.target("http://localhost:8080/RestService/rest");
	protected HttpSession session = (HttpSession) FacesContext
			.getCurrentInstance().getExternalContext().getSession(false);
	protected HttpBasicAuthFilter filter;

	protected Gson gson = new Gson();
	protected Response res;

	abstract T save(T t);

	abstract T update(T t);

	abstract List<T> getAll();

	abstract void remove(T t);

	abstract T find(T t);
}
