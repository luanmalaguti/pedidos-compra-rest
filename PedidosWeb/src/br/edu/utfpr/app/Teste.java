package br.edu.utfpr.app;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class Teste {
	public static void main(String[] args) {
		Client client = ClientBuilder.newClient();
		
		WebTarget target = client.target("http://localhost:8080/RestService/rest");
		
		Response res = target.path("produto").request(MediaType.APPLICATION_JSON).get();
		//String r = res.readEntity(String.class);
		//System.out.println(r);
		printResposta(res);
	}
	
	private static void printResposta(Response res) {
		System.out.println("Status: "+res.getStatus());
		System.out.println("Status info: "+res.getStatusInfo());
		System.out.println("Cabeçalho: "+res.getStringHeaders());
		System.out.println("Resposta: "+res.readEntity(String.class));
		
		System.out.println("####################################");
	}
}
